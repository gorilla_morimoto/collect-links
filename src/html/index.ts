/**
 * のちのち扱いやすいようにHTMLを変換する
 */
const sanitizeHTML = (html: string): string => {
  return html.replace('&quot;', '"');
};

const extractAtag = (html: string): string[] => {
  const re = /<a.*?>/g;
  const atags = html.match(re);
  return atags === null ? [] : atags;
};

const extractHREF = (atag: string): string => {
  const re = /href="(.*?)"/;
  const hrefAttribute = atag.match(re);
  return hrefAttribute === null ? '' : hrefAttribute[1];
};

/**
 * 文字列から拡張子を抽出する
 * 拡張子がなければ空文字列を返す
 */
export const extractExtension = (s: string) => {
  if (s.endsWith('/')) {
    return '';
  }

  const extensionStart = s.lastIndexOf('.');
  if (extensionStart === -1) {
    return '';
  }

  return s.slice(extensionStart + 1);
};

/**
 * HTMLの<a>のhrefの値を抽出する
 * 空文字は除外するが、URLのバリデーションは行わない
 */
export const extractURL = (html: string): string[] => {
  const sanitizedHTML = sanitizeHTML(html);
  return extractAtag(sanitizedHTML)
    .map(extractHREF)
    .filter(href => href !== '');
};
