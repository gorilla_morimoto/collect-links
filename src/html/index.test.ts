import { extractURL, extractExtension } from './index.js';

test('extract url', () => {
  const testCases = [
    {
      input: '<a href="/about/">About Us</a>',
      expected: ['/about/']
    },
    {
      input: '<a href="">About Us</a>',
      expected: []
    },
    {
      input: '<a href="dummy">About Us</a>',
      expected: ['dummy']
    },
    {
      input: '<a href="//dummy/test.html">About Us</a>',
      expected: ['//dummy/test.html']
    },
    {
      input: '<a href="#top">About Us</a>',
      expected: ['#top']
    },
    {
      input: '<a href="/about/?v=xyz">About Us</a>',
      expected: ['/about/?v=xyz']
    }
  ];

  testCases.forEach(testCase => {
    const { input, expected } = testCase;
    const actualValue = extractURL(input);

    expect(expected.sort().join(',')).toEqual(actualValue.sort().join(','));
  });
});

test('extract extension', () => {
  expect(extractExtension('s.html')).toBe('html');
  expect(extractExtension('/tmp/a.s.html')).toBe('html');
  expect(extractExtension('s.')).toBe('');
  expect(extractExtension('/bak/')).toBe('');
  expect(extractExtension('')).toBe('');
});
